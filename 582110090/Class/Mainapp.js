
const Vec = require('./Class')
const Cal = new Vec()
console.log(Cal.display(Cal.plus(1, 2), (Cal.plus(2, 3))));
// → Vec{x: 3, y: 5}
console.log(Cal.display(Cal.minus(1, 2), (Cal.minus(2, 3))));
// → Vec{x: -1, y: -1}
console.log(Cal.length(3, 4));
// → 5