class Vec {     //ref all name from exercises
    plus(x, y) {
        return x + y
    }
    minus(x, y) {
        return x - y
    }
    display(x, y) {
        return 'Vec{x: ' + x + ', y: ' + y + '}'
    }
    length(x, y) {
        return 'Vector length = '+Math.sqrt((x * x) + (y * y))
    }
}
module.exports = Vec