
function every(arrays, test) {
  let check = true
  for(array of arrays){
    if(test(array)){
      check = true
    }else {
      check = false
    }
  }
  return check
}

console.log(every([1, 3, 5], n => n < 10)) // true
console.log(every([2, 4, 16], n => n < 10)) // false
console.log(every([], n => n < 10)) // true