var chessBoard = "",
  size,
  printChess,
  playChess;

playChess = () => {
  size = prompt("Please enter chess size");
  for (var row = 0; row < size; row++) {
    printChess = row % 2 ? "# " : " #";
    for (var col = 0; col < size / 2; col++) {
      chessBoard += printChess;
    }
    chessBoard += "\n";
  }

  console.log(chessBoard);
  chessBoard = "";
};

// var input = 8;
// var printRow = "";

// function printChess(input) {
//     console.log(input);
//     for (let row = 1; row <= 8; row++) {
//         if(row%2 === 0){
//             for (let col = 1; col <= 8; col++) {
//                 if(col%2 === 0){
//                     printRow += "#"
//                 }else {
//                     printRow += " "
//                 }
//                 printRow = "\n"
//             }
//         }else {
//             for (let col = 1; col <= 8; col++) {
//                 if(col%2 === 0){
//                     printRow += "#"
//                 }else {
//                     printRow += " "
//                 }
//                 printRow = "\n"
//             }

//         }

//         console.log(printRow);
//     }

// }
