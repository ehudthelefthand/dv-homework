const Vector = require('./Vector')

Object.defineProperty(Vector.prototype, 'length', {
        get: function () {
                return Math.sqrt(Math.pow(this.y, 2) + Math.pow(this.x, 2));
        }
})

console.log(new Vector(1, 2).plus(new Vector(2, 3)));
// → Vec{x: 3, y: 5}
console.log(new Vector(1, 2).minus(new Vector(2, 3)));
// // → Vec{x: -1, y: -1}
console.log(new Vector(3, 4).length);
// // → 5