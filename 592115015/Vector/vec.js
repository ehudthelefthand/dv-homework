class Vec {

    constructor(x, y) {
        this.x = x;
        this.y = y;
        //version 1
        this.length = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));       // used pythagoras to find length 
        //version 2
        // this.length = Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));   // round it
    }


    plus(Vec2) {
        const newVec = new Vec(Vec2.x + this.x, Vec2.y + this.y);
        delete newVec["length"];            // delete it because i want this obj to show the same value
        return newVec;                     // as the example
    }
    minus(Vec2) {
        const newVec = new Vec(this.x - Vec2.x, this.y - Vec2.y);
        delete newVec["length"];            // delete it because i want this obj to show the same value
        return newVec;                     // as the example
    }

    // If we want to know length just used .length

}
module.exports = Vec 
