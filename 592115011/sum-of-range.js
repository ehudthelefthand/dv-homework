
var range2Para = function(a,b) {
    var arr = [];
    if (b > a)
      for (var i = a; i <= b; i++)
        arr.push(i);
    else
      for (var j = a; j >= b; j--)
        arr.push(j);
    return arr;
  }
  
  //////////////////////////////////////////////////
  var range3Para = function(a, b, step) {
    var arr = [];
    if (b > a)
      for (var i = a; i <= b; i = i + step)
        arr.push(i);
    else
      for (var j = a; j >= b; j = j + step)
        arr.push(j);
    return arr;
  }
  
//////////////////////////////////////////
  var sum = function(arr) {
    var sumVar = 0;
    for (var i = 0; i < arr.length; i++) 
      sumVar = sumVar + arr[i];
    return sumVar;
  }
  
  console.log(sum(range2Para(1, 10)));
  // → 55
  
  var stepArray = range3Para(5, 2, -1);
  for (var i = 0; i < stepArray.length; i++) {
    console.log(stepArray[i]);
  } 
  
  // → [5, 4, 3, 2]